import 'package:flutter/material.dart';
import 'package:nav_app/Screens/Login/components/backbtn.dart';
import 'package:nav_app/Screens/Login/components/login_success_image.dart';
import 'package:nav_app/Screens/Login/login_screen.dart';
import 'package:nav_app/Screens/Signup/components/register_success_image.dart';
import 'package:nav_app/Screens/Welcome/components/login_btn.dart';
import 'package:nav_app/responsive.dart';

import '../../../components/background.dart';
import '../Welcome/components/login_signup_btn.dart';

class RegisterSuccess extends StatelessWidget {
  const RegisterSuccess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Background(
      child: SingleChildScrollView(
        child: SafeArea(
          child: Responsive(
            desktop: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Expanded(
                  child: LoginSucessPage(),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(
                        width: 450,
                        child: LoginAndSignupBtn(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            mobile: const MobileRegisterSuccessScreen(),
          ),
        ),
      ),
    );
  }
}

class MobileRegisterSuccessScreen extends StatelessWidget {
  const MobileRegisterSuccessScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const RegisterSucessPage(),
        Row(
          children: const [
            Spacer(),
            Expanded(
              flex: 8,
              child: LoginBtn(),
            ),
            Spacer(),
          ],
        ),
      ],
    );
  }
}

