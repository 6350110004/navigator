import 'package:flutter/material.dart';
import 'package:nav_app/constants.dart';

class RegisterSucessPage extends StatelessWidget {
  const RegisterSucessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "!!!Welcome!!!",
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        Padding(padding: EdgeInsets.all(4)),
        Text(
          " 👍 👍",
          style: TextStyle(
            fontSize: 38,
            fontWeight: FontWeight.bold,
            color: Color(0xFFEF2828),
          ),
        ),
        SizedBox(height: defaultPadding * 2),
        Row(
          children: [
            Spacer(),
            Expanded(
              flex: 8,
              child: Icon(
                Icons.how_to_reg,
                size: 200,
                color: Colors.black,
              ),
            ),
            Spacer(),
          ],
        ),
        SizedBox(height: defaultPadding * 2),
      ],
    );
  }
}
