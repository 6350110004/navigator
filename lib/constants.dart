import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFE700FF);
const kPrimaryLightColor = Colors.green;

const double defaultPadding = 16.0;
